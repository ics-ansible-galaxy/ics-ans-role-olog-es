# ics-ans-role-olog-es

Ansible role to install olog-es.

## Example Playbook

```yaml
---
olog_es_network: olog-es-network

olog_es_container_name: olog-es
olog_es_image: registry.esss.lu.se/ics-software/ess-olog-product:1.0.3
olog_es_image_pull: true
olog_es_published_ports: []
olog_es_env: {}
olog_es_frontend_rule: "Host:{{ olog_es_external_hostname }};PathPrefix:/Olog"
olog_es_container_uid: 1000
olog_es_container_gid: 1000

olog_es_mongodb_container_name: mongodb
olog_es_mongodb_image: mongo:4.2.3
olog_es_mongodb_volume: mongodb-data
olog_es_mongodb_backup_dir: /var/olog/backup/mongodb
olog_es_mongodb_env: {}

olog_es_elasticsearch_container_name: elasticsearch
olog_es_elasticsearch_image: docker.elastic.co/elasticsearch/elasticsearch:6.8.13
olog_es_elasticsearch_volume: elasticsearch-data
olog_es_elasticsearch_backup_dir: /var/olog/backup/elasticsearch
olog_es_elasticsearch_env:
  discovery.type: single-node
  bootstrap.memory_lock: "true"
  ES_JAVA_OPTS: -Xms512m -Xmx512m
  path.repo: "/backup"

olog_es_external_hostname: "{{ ansible_fqdn }}"
olog_es_web_frontend_rule: "Host:{{ olog_es_external_hostname }}"
olog_es_cors_origins: "https://{{ olog_es_external_hostname }}"

olog_es_web: true
olog_es_web_container_name: olog-es-web
olog_es_web_image: registry.esss.lu.se/ics-docker/olog-web:master
olog_es_web_image_pull: true
olog_es_web_env: {}
olog_es_web_published_ports: []

olog_es_kafka_container_name: kafka
olog_es_kafka_image: confluentinc/cp-kafka:7.0.1
olog_es_kafka_properties_file: /etc/kafka/server.properties
olog_es_kafka_env:
  KAFKA_BROKER_ID: "1"
  KAFKA_LISTENERS: "INTERNAL://0.0.0.0:{{ olog_es_kafka_internal_port }},EXTERNAL://0.0.0.0:{{ olog_es_kafka_external_port }}"
  KAFKA_ZOOKEEPER_CONNECT: zookeeper:2181
  KAFKA_LISTENER_SECURITY_PROTOCOL_MAP: INTERNAL:PLAINTEXT,EXTERNAL:PLAINTEXT
  KAFKA_ADVERTISED_LISTENERS: "INTERNAL://kafka:{{ olog_es_kafka_internal_port }},EXTERNAL://{{ olog_es_external_hostname }}:{{ olog_es_kafka_external_port }}"
  KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR: "1"
  KAFKA_GROUP_INITIAL_REBALANCE_DELAY_MS: "0"
  KAFKA_CONFLUENT_LICENSE_TOPIC_REPLICATION_FACTOR: "1"
  KAFKA_CONFLUENT_BALANCER_TOPIC_REPLICATION_FACTOR: "1"
  KAFKA_TRANSACTION_STATE_LOG_MIN_ISR: "1"
  KAFKA_TRANSACTION_STATE_LOG_REPLICATION_FACTOR: "1"
  KAFKA_INTER_BROKER_LISTENER_NAME: INTERNAL
olog_es_kafka_port: 9092

olog_es_zookeeper_container_name: zookeeper
olog_es_zookeeper_image: confluentinc/cp-zookeeper:7.0.1
olog_es_zookeeper_env:
  ZOOKEEPER_CLIENT_PORT: "2181"
  ZOOKEEPER_TICK_TIME: "2000"

olog_es_notify_url: https://notify.esss.lu.se/api/v2/services
olog_es_web_root_url: https://olog-es-lab.cslab.esss.lu.se/logs
olog_es_service_mapping_file: /var/olog_json/ess-notify-mapping.json
olog_es_service_mapping_file_data: 
  Test: 71305561-dead-4d09-93d6-a87b6bb7730f


```

## License

BSD 2-clause
