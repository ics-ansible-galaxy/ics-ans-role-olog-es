import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_containers(host):
    with host.sudo():
        assert host.docker("olog-es").is_running
        assert host.docker("elasticsearch").is_running
        assert host.docker("mongodb").is_running
        assert host.docker("olog-es-web").is_running


def test_api(host):
    cmd = host.run("curl --insecure --fail https://" + host.ansible.get_variables()['inventory_hostname'] + "/Olog/properties")
    assert cmd.rc == 0
    assert "\"state\":\"Active\"" in cmd.stdout


def test_olog_web(host):
    cmd = host.run("curl --insecure --fail https://" + host.ansible.get_variables()['inventory_hostname'])
    assert cmd.rc == 0
    assert "<title>Olog</title>" in cmd.stdout
